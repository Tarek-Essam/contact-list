'use strict'

const assert = require('assert');
const request = require('supertest');
const app = require('../../app/app');
const Contact = require('../../app/models/contact');

describe("Contacts Controller", () => {

    beforeEach((done) => {
        Contact.remove({email:"newuser@gmail.com"}).then(() => done());
    });

    it("Post to /contacts/addContact creates new contact", (done) => {
        request(app)
          .post('/contacts/addContact')
          .send({
              email:"newuser@gmail.com",
              firstName:"new",
              lastName:"user",
              mobileNumber:"01068347479", 
              userId:1,
              autherization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0",
              deviceToken:"dnfkjnjkdnjksnsknkngklngsklgnkgnk",
              fingerPrint:"123456789"             
          })
          .end((err, res) => {
              assert(res.body.statusCode === 200);
              Contact.findOne({email:"newuser@gmail.com"}).then(contact => {                  
                  assert(contact.mobileNumber === "01068347479");
                  done();
              });                   
          });
    });

    it("Post to /contacts/getList to get list of contacts", (done) => {
        request(app)
          .post('/contacts/getList')
          .send({
              pageNum:1,
              character:"a",
              autherization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0",
              deviceToken:"dnfkjnjkdnjksnsknkngklngsklgnkgnk",
              fingerPrint:"123456789" 
          })
          .end((err, res) => {
              const docs = res.body;
              assert(docs.length <= 2);
              assert(docs[0].firstName[0].toLowerCase() === "a");
              done();
          })
    });

    it("Post to /contacts/getRecentList to get recent contacts", (done) => {
        request(app)
          .post('/contacts/getRecentList')
          .send({
            autherization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0",
            deviceToken:"dnfkjnjkdnjksnsknkngklngsklgnkgnk",
            fingerPrint:"123456789" 
          })
          .end((err, res) => {
              done();
          });
    });

    it("Test the autherization middleware", (done) => {
        request(app)
        .post('/contacts/getRecentList')
        .send({
          autherization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0",
          deviceToken:"dnfkjnjkdnjksnsknkngklngsklgnkgnk",          
        })
        .end((err, res) => {
            assert(res.body.message === "you are not autherized");
            done();
        });
    });

    it("Test validation of email", (done) => {
        request(app)
          .post('/contacts/addContact')
          .send({
              email:"newuser@gmail",
              firstName:"new",
              lastName:"user",
              mobileNumber:"01068347479", 
              userId:1,
              autherization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0",
              deviceToken:"dnfkjnjkdnjksnsknkngklngsklgnkgnk",
              fingerPrint:"123456789"             
          })
          .end((err, res) => {
              assert(res.body.statusCode === 400);
              assert(res.body.message == "failed to add contact");
              assert(res.body.errors.email.message === "not a valid email.")
              done();                             
          });        
    });
});