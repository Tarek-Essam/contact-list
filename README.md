# Contact List

create three APIs to create and view contacts using nodejs express framwork and mongodb. The APIs were tested using mocha framework.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* node v8.10.0 
* mongodb v3.2.19


### Installing

1- clone the repo

2- inside the project run
```
npm install
```

3- create a database named **vapulus**

4- create user for the database with the name **islam** and password **123456**:
```
use vapulus
db.creatUser({user:"islam", pwd:"123456", role:"readWrite"})
```
you can change any of the above or run the server without --auth option just change the varialble DB_CONN in **.env** file

5- run the mongod server with --auth option

```
mongod --auth
```

6- seed the database with initial data 
```
npm run seed
```

## Run the server
```
npm start
```

## Running the tests

```
npm run test
```

## APIs Descriptions

* **/contacts/addContact**--    **method** : post--
   **body** : {
   	email : String,
	mobileNumber : String,
	firstName : String,
	lastName : String,
	autherization : String,
	deviceToken : String,
	fingerPrint : String
   }
   
* **/contacts/getList**--      **method** : post--
   **body** : {
   	pageNum : Number,
	character : String,
	autherization : String,
	deviceToken : String,
	fingerPrint : String
   }

* **/contacts/getRecentList**--      **method** : post--
   **body** : { 
	autherization : String,
	deviceToken : String,
	fingerPrint : String
   }
* autherization, deviceToken, and fingerPrint for the users are in userAuth.json file.

## Project Structure

* **app directory** the source code for the application.
* **data** seed the database collections with initial data.
* **test** for testing the APIs using mocha framework.
* **index.js** the entry point for the app.










