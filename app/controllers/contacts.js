'use strict'

const express = require('express');
const Contact = require('../models/contact');
const uniqueContactMid = require('../middlewares/uniqueContact');
const router = express.Router();

router.post('/getList', (req, res) => {

    const pageNum = req.body.pageNum;
    const character = req.body.character;

    Contact.getList(req.body.userId, pageNum, character)
    .then((contacts) => res.send(contacts.docs))
    .catch((error) => res.send(error));
    
});

router.post('/getRecentList', (req, res) => {

    Contact.getRecentList(req.body.userId)
    .then((contacts) => res.send(contacts))
    .catch((error) => res.send(error));

});

router.post("/addContact", uniqueContactMid, (req, res) => {
   
    Contact.addContact(req.body)
    .then((contact) => res.send({
        statusCode : 200,
        message : "contact added successfully",
        data : contact
    }))
    .catch((validationErrors) => res.send({
        statusCode : 400,
        message : "failed to add contact",
        errors : validationErrors.errors
    }));
   
});

module.exports = router;
