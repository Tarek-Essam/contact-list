'use strict'

const contactMid = require('./controllers/contacts')

module.exports = (app) => {
    app.use('/contacts', contactMid);    
};