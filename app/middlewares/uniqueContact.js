'use strict'

const Contact = require('../models/contact');

module.exports = function (req, res, next) {

    Contact.getContact(req.body.email, req.body.userId)
    .then((contact) => {
        if(contact){
            res.send({
                statusCode:400,
                message: "contact already exist"
            })
        }else{
            next();
        }
    })        
}