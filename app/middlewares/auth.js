'use strict'

const users = require('../../userAuth');
const User = require('../models/user');

module.exports = {

    autherize : function (req, res, next) {

        const autherization = req.body.autherization;
        const deviceToken = req.body.deviceToken;
        const fingerPrint = req.body.fingerPrint;
    
        if(autherization && deviceToken && fingerPrint){
            users.forEach(user => {
                if(user.autherization == autherization && user.deviceToken == deviceToken && user.fingerPrint == fingerPrint){
                    req.body.user = user.name;
                }
            });        
        }
    
        if(req.body.user){
            next();
        }else{        
            res.send({
                statusCode : 403,
                message : "you are not autherized"
            });
        }           
    },

    getUserId : function (req, res, next) {

        User.getUserId(req.body.user)
        .then((user) => {
            req.body.userId = user._id;
            next();
        });        
    }
}