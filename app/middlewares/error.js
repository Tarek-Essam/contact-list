'use strict'

module.exports = function (req, res, next) {
    res.status(404).send({
        statusCode : 404,
        message : 'Opps....The requested page does not exist'
    });    
}