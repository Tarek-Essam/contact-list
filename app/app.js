'use strict'

const mongoose = require('mongoose');
const express = require('express');
const bodyparser = require('body-parser').json();
const routes = require('./routes');
const auth = require('./middlewares/auth');
const errorMid = require('./middlewares/error');

// load environment variable from .env file
require('dotenv').config();

mongoose.connect(process.env.DB_CONN);

require("./models/contact");

const app = express();

// Middleware to parse the body and check autherization
app.use(bodyparser);
app.use(auth.autherize);
app.use(auth.getUserId);

// routes of the application
routes(app);

// Middleware to catch error
app.use(errorMid);

module.exports = app;