'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema ({
    _id:Number,
    name:{
        type:String
    }
});

userSchema.statics.getUserId = function (name) {
    return User.findOne({name:name});    
}

const User = mongoose.model('user', userSchema);

module.exports = User;