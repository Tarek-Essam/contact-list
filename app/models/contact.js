'use strict'

const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const mongoosePaginate = require('mongoose-paginate');
const validator = require('validator');
const Schema = mongoose.Schema;

const contactSchema = new Schema({
    email:{
        type: String,
        required: [true, "email is Required."],        
        validate: {
            validator : (email) => validator.isEmail(email),
            message : "not a valid email."
        }        
    },
    mobileNumber:{
        type: String,
        validate: {
            validator: (mobileNumber) => validator.isMobilePhone(mobileNumber, 'any'),
            message : "not a vaild phone number"
        },
        required: [true, "mobile number is required."]
    },
    firstName:{
        type: String,
        required: [true, "firstName is Required."],
        validate: {
            validator: (firstName) => validator.isAlpha(firstName),
            message : "only alphabetical letters allowed."
        }
    },
    lastName:{
        type: String,
        required: [true, "lastName is Required."],
        validate: {
            validator: (firstName) => validator.isAlpha(firstName),
            message : "only alphabetical letters allowed."
        }
    },
    userId:{
        type:Number,
        ref:'user'
    },
    createdAt:{
        type:Date,
        default:Date.now
    } 
});

contactSchema.plugin(mongoosePaginate);
autoIncrement.initialize(mongoose.connection);
contactSchema.plugin(autoIncrement.plugin, {model:'contactSchema', startAt:1, field:'_id', incrementBy: 1});

contactSchema.statics.addContact = function (contact) {

    const newContact = new Contact({
        email:contact.email,
        mobileNumber:contact.mobileNumber,
        firstName:contact.firstName,
        lastName:contact.lastName,
        userId:contact.userId
    });

    const validationResult = newContact.validateSync();

    if(!validationResult)        
        return newContact.save();        
    else
        return Promise.reject(validationResult);   
    
}

contactSchema.statics.getList = function (userId, pageNum, character) {
    
    const Contact = mongoose.model('contact');
    const startWith = new RegExp("^" + character, "i");    

    return Contact.paginate(
        {userId:userId, firstName:startWith},
        {sort:{firstName:1}, page: pageNum, limit: 4}
    );            
}

contactSchema.statics.getRecentList = function (userId) {

    return Contact.find({userId:userId})
                  .sort({createdAt:-1})
                  .limit(5);    
}

contactSchema.statics.getContact = function (email, userId){

    return Contact.findOne({ email:email, userId:userId });
}

const Contact = mongoose.model("contact", contactSchema);

module.exports = Contact;