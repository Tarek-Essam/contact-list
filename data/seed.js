'use strict'

require('dotenv').config();

var usersData = require('./users');
var contactsData = require('./contacts');

const mongoose = require('mongoose');

const User = require('../app/models/user');
const Contact = require('../app/models/contact');

mongoose.connect(process.env.DB_CONN);

User.remove({})
  .then(() => Contact.remove())
  .then(() => User.insertMany(usersData))
  .then(() => initContacts())

  async function initContacts() {   
    for (let i = 0; i < contactsData.length; i++) {
      await Contact.addContact(contactsData[i])      
    }       
  }